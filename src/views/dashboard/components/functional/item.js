export default {
  name: 'dashboard-item',
  functional: true,
  render(h, context) {
    const { props: { text } } = context
    console.log('dashboard-item','functional', 'render', text)
    if (text) {
      return h('li', {}, text)
    }
    return h('li', {}, '我没有什么可说的')
  }
}