/* 
import Item from './item'
export default {
  name: 'dashboard-list',
  // functional: true,
  components: { Item },
  props: {
    list: {
      type: Array
    }
  },
  render(h) {
    // console.log('dashboard-list', 'render', this.list)
    // const { props:
    //   {
    //     list
    //   }
    // } = context
    // const { list } = this.$props
    const list = this.list;
    console.log(list)
    return h(
      'ul',
      {},
      list.map((text) => h(Item, { props: { text } }))
    )
  }
}
 */

import Item from './item'
export default {
  name: 'dashboard-list',
  functional: true,
  components: { Item },
  render(h, context) {
    const { props:
      {
        list
      }
    } = context
    console.log('dashboard-list', 'functional', 'render', list)
    return h(
      'ul',
      {},
      list.map((text) => h(Item, { props: { text } }))
    )
  }
}
