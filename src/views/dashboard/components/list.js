import Item from './item'
export default {
  name: 'dashboard-list',
  components: { Item },
  props: {
    list: {
      type: Array
    }
  },
  render(h) {
    const list = this.list;
    console.log('dashboard-list', 'render', list)
    return h(
      'ul',
      {},
      list.map((text) => h(Item, { props: { text } }))
    )
  }
}
