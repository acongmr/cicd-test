export default {
  name: 'dashboard-item',
  props: {
    text: {
      type: String
    }
  },
  render(h) {
    const text = this.text
    console.log('dashboard-item', 'render', text)
    if (text) {
      return h('li', {}, text)
    }
    return h('li', {}, '我没有什么可说的')
  }
}